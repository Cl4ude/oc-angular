import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  secondes: number;
  counterSubsciption: Subscription;

  constructor() {}

  ngOnInit() {
    const counter = Observable.interval(1000);
    this.counterSubsciption = counter.subscribe(
      (value) => {this.secondes = value},
      (error) => {console.log('Observable error :' +error)},
      () => {console.log('Observable complete')}
    );
  }

  ngOnDestroy(){
    this.counterSubsciption.unsubscribe();
  }
  
}